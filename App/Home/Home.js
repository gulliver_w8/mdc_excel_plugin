﻿/* Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. 
    See full license at the bottom of this file. */

/// <reference path="../App.js" />

(function () {
	"use strict";

	// The initialize function must be run each time a new page is loaded
	Office.initialize = function (reason) {
		$(document).ready(function () {
		    app.initialize();

		    // If not using Excel 2016, return
		    if (!Office.context.requirements.isSetSupported('ExcelApi', '1.1')) {
		        app.showNotification("Need Office 2016 or greater", "Sorry, this app only works with newer versions of Excel.");
		        return;
		    }
		    populateFilters()
			$('#search-button').click(searchAnswersview);
		});
	};

	// Populates the workbook with some sales data in three new sheets
	function populateFilters() {

		// Run a batch operation against the Excel object model
		Excel.run(function (ctx) {

			// Run all of the above queued-up commands, and return a promise to indicate task completion
			return ctx.sync().then(function () {

				// For each sheet, add a checkbox in the UI
				//for (var i = 0; i < sheets.items.length; i++) {
					// The name property has been loaded already
					//var checkboxName = sheets.items[i].name;
			    var $input = $('<select id="user">' +
                 '<option value="T1">Utente Test 1</option>' +
                 '<option value="T2">Utente Test 2</option>' +
                 '<option value="T3">Utente Test 3</option>' +
                 '<option value="T4" selected>Utente Test 4</option>' +
                '</select>');
			  
			    $input.appendTo($("#users-set"));

			    var $input = $('<select id="app">' +
                '<option value="Simon">Simon</option>' +
                '<option value="Joe">Joe</option>' +
                '<option value="GET_MISSION">Get Mission</option>' +
               '</select>');

					$input.appendTo($("#apps-set"));
				//}
			})
		})
		.catch(function (error) {
			// Always be sure to catch any accumulated errors that bubble up from the Excel.run execution
			app.showNotification("Error: " + error);
			console.log("Error: " + error);
			if (error instanceof OfficeExtension.Error) {
				console.log("Debug info: " + JSON.stringify(error.debugInfo));
			}
		});
	}


	// Consolidate data from sheets into a new dashboard sheet
	function searchAnswersview() {

		// Run a batch operation against the Excel object model
	    Excel.run(function (ctx) {

	        // Disable the controls while sending data 
	        $('.disable-while-sending').prop('disabled', true);
	        var  azure =  "https://mdcrest.azurewebsites.net" ;
	        var local = "https://localhost:44300";
	        var $url = azure + "/api/answersview/" + $("#app").val() + "/" + $("#user").val();

	        $.ajax({
	            type: "GET",
	            contentType: "text/javascript",
	            url: $url
	        }).done(function (data) {
	            try{
	                if (data.length > 0) {
	                    var fieldNames = [];
	                    var readData = new Array();
	                    
	                    for (var i = 0; i < data.length; i++)
	                    {
	                        var tmp = new Array();
	                        jQuery.each(data[i], function (name, val) {
	                            if (name.indexOf("FieldSpecified") < 0)  {
	                                if (i == 0)
	                                    fieldNames.push(name.replace("Field", ""));
	                                tmp.push(val);
	                               
	                            }
	                        })
                            if(tmp)
	                            readData.push(tmp);
	                    };

	                    // Create a proxy object for the worksheets collection
	                    var sheets = ctx.workbook.worksheets;

	                    // Queue commands to add three new worksheets to the workbook
	                    var sheet1 = sheets.add("Answersview");

	                    // Queue commands to set the title in the sheet and format it
	                    sheet1.getRange("A1:A1").values = "AnswersView";
	                    sheet1.getRange("A1:A1").format.font.name = "Century";
	                    sheet1.getRange("A1:A1").format.font.size = 26;

	                    sheet1.activate();

	                   var LabelsTopLeftCell = sheet1.getRange("A2");
	                   var LabelsRange = LabelsTopLeftCell.getBoundingRect(
                           LabelsTopLeftCell.getOffsetRange(0, fieldNames.length - 1));

	                   // LabelsRange.format.fill.color = "green";
	                    LabelsRange.values = [fieldNames];

	                    var DataTopLeftCell = sheet1.getRange("A3");
	                    var DataRange = DataTopLeftCell.getBoundingRect(
                            DataTopLeftCell.getOffsetRange(data.length -1, fieldNames.length - 1));

	                    DataRange.values = readData;

	                    ctx.workbook.tables.add('Answersview!A2:' + columnName(fieldNames.length -1) + (data.length +2), true);

	                }
	            } catch (error) {
	                // Always be sure to catch any accumulated errors that bubble up from the Excel.run execution
	                app.showNotification("Error: " + error);
	                console.log("Error: " + error);
	                if (error instanceof OfficeExtension.Error) {
	                    console.log("Debug info: " + JSON.stringify(error.debugInfo));
	                }
	            }

	        }).fail(function (status) {
	            app.showNotification('Error', 'Could not communicate with the server.');
	        }).always(function (data) {
	            $('.disable-while-sending').prop('disabled', false);
	        }).then(ctx.sync);

			// Run the queued-up commands
	        return ctx.sync();
	
		})
		.catch(function (error) {
		    // Always be sure to catch any accumulated errors that bubble up from the Excel.run execution
		    app.showNotification("Error: " + error);
		    console.log("Error: " + error);
		    if (error instanceof OfficeExtension.Error) {
		        console.log("Debug info: " + JSON.stringify(error.debugInfo));
		    }
		});
	}

	/**
	  * Returns the column name based on a zero-based column index.
	  * For example, columnName(4) = 5th column = "E". Meanwhile, columnName(1000) = 1001st column = "ALM".
	  * @param index Zero-based column index.
	  * @returns {String} Locale-independent column name (e.g., a string comprised of one or more letters in the range "A:Z").
	  */
	function columnName(index) {
		if (typeof index !== 'number' || isNaN(index) || index < 0) {
			throw new OfficeExtension.RuntimeError("InvalidArgument", "The parameter for Excel.columnName(x) must be positive and numeric.", [], { errorLocation: "Excel.Util.columnName" });
		}
		var letters = [];
		while (index >= 0) {
			letters.push(getSingleLetter(index % 26));
			index = Math.floor(index / 26) - 1;
		}
		return letters.reverse().join('');
		function getSingleLetter(zeroThrough25Index) {
			return String.fromCharCode(zeroThrough25Index + 65); // ASCII code for "A" is 65
		}
	}



})();
